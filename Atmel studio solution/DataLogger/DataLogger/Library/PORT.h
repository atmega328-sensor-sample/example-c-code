/*
 * PORT.h
 *
 * Created: 05-03-2019 15:56:08
 *  Author: NISI
 */ 


#ifndef PORT_H_
#define PORT_H_

void initPort(char DataDirReg, char BitNo);
void enablePullup(char DataReg, char BitNo);

#endif /* PORT_H_ */