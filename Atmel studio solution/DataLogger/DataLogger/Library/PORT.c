/*
 * PORT.c
 *
 * Created: 05-03-2019 15:56:48
 *  Author: NISI
 */ 

#include <avr/io.h> // avr macros
#include "PORT.h"

//initializes a ports bit
void initPort(char DataDirReg, char BitNo) {
	
	if (DataDirReg == DDRB) {
		DDRB |= BitNo;
	}
	if (DataDirReg == DDRC) {
		DDRC |= BitNo;
	}
	if (DataDirReg == DDRD) {
		DDRD |= BitNo;
	}
	else {
		//do nothing
	}
}

//enable pull up on port input bit
void enablePullup(char DataReg, char BitNo) {
	if (DataReg == PORTB) {
		PORTB |= BitNo;
	}
	if (DataReg == PORTC) {
		PORTC |= BitNo;
	}
	if (DataReg == PORTD) {
		PORTD |= BitNo;
	}
	else {
		//do nothing
	}
}
