# example c code

This repository contains the Atmel Studio C project "DataLogger".

/*
 * DataLogger.c
 *
 * This example gets or sets port values via commands from UART. 
 * UART is using 9600 BAUD, 8 databits, 1 stop bit, no parity.
 *
 * it reads adc value from from the specified ADC port.
 * It detects button presses on specified GPIO port and pin
 * It manipulates a led on specified GPIO port and pin
 *
 * UART commands:
 *
 * led1on (turns on led1, replies with led1=1)
 * led1off (turns off led1, replies with led1=0)
 * getbtn1 (reads button 1 status, replies with btn1=0 or btn=1, resets status if btn1=1)
 * getadcval (returns raw adc value from ADC)
 *
 * Created: 14-01-2019 12:44:50
 * Modified: 14-02-2019 19:39:33
 * Author : Nikolaj Simonsen
 *
 */  

